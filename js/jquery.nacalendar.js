/**
 * naCalendar
 * by Nathalie Arduini
 * v0.1.0
 * */
(function($) {

    $.naCalendar = function(element, options) {

        // plugin's default options
        // this is private property and is accessible only from inside the plugin
        var defaults = {

            debug: false,

            view: 'month',
            date: moment(),
            events: [],
            url: null,
            urlParams: {},
            dayTopHour: '8:00',
            loading: '<i class="fa fa-spinner fa-spin"></i> Loading...',
            totalMaxEvents: 100,
            failsafeMaxPerDay: 50,

            onEventClick: function($el) {
                if (plugin.settings.debug) {
                    log('onEventClick');
                    log($el);
                    log('================');
                }
            },
            onDaySelect: function(range) {
                if (plugin.settings.debug) {
                    log('onDaySelect');
                    log(range);
                    log('================');
                }
            },
            onAjaxError: function(response) {
                if (plugin.settings.debug) {
                    log('onAjaxError');
                    log(response);
                    log('================');
                }
            },
            onInvalidJSON: function(response) {
                if (plugin.settings.debug) {
                    log('onInvalidJSON');
                    log(response);
                    log('================');
                }
            },
            onTitleChange: function() {},
            onTooManyEvents: function(max, total) {
                alert("Sorry, too many events.\nThe calendar would be bloated.");
            }

        };

        var plugin = this;
        plugin.settings = {};

        var $element = $(element);
            // reference to the jQuery version of DOM element
        element = element; // reference to the actual DOM element
        // the "constructor" method that gets called when the object is created
        plugin.init = function() {

            // the plugin's final properties are the merged default and 
            // user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

            // code goes here
            plugin.view = plugin.settings.view;
            plugin.date = plugin.settings.date == 'today' ? moment() : moment(plugin.settings.date);
            plugin.events = plugin.settings.events;
            plugin.urlParams = plugin.settings.urlParams;

            plugin.mousedown = false;

            setRange();
            wireframe();
            initEvents();
            getData();
        };

        // public methods
        // these methods can be called like:
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // element.data('naCalendar').publicMethod(arg1, arg2, ... argn) from outside 
        // the plugin, where "element" is the element the plugin is attached to;
        plugin.previous = function() {
            switch (plugin.view) {
                case 'month':
                    plugin.date.subtract(1, 'months');
                    break;
                case 'week':
                    plugin.date.subtract(1, 'weeks');
                    break;
                case 'day':
                    plugin.date.subtract(1, 'days');
                    break;
            }
            rebuild();
        };

        plugin.next = function() {
            switch (plugin.view) {
                case 'month':
                    plugin.date.add(1, 'months');
                    break;
                case 'week':
                    plugin.date.add(1, 'weeks');
                    break;
                case 'day':
                    plugin.date.add(1, 'days');
                    break;
            }
            rebuild();
        };

        plugin.goto = function(date) {
            plugin.date = date == 'today' ? moment() : moment(date);
            rebuild();
        };

        /**
         * @param view day | week | month
         * @param date optional to change the view AND change the date
         **/

        plugin.setView = function(view, date) {
            if (date !== undefined) {
                plugin.date = moment(date);
            }
            plugin.view = view;
            rebuild();
        };

        plugin.setParams = function(urlParams) {
            plugin.urlParams = urlParams;
            getData();
        };

        plugin.refresh = function() {
            getData();
        };

        plugin.title = function() {
            var result;
            switch (plugin.view) {
                case 'month':
                    result = plugin.date.format('MMMM YYYY');
                    break;
                case 'week':
                    if (plugin.range.start.isSame(plugin.range.end, 'year')) {
                        result = plugin.range.start.format('DD/MM') + ' - ' + plugin.range.end.format('DD/MM/YYYY');
                    } else {
                        result = plugin.range.start.format('DD/MM/YYYY') + ' - ' + plugin.range.end.format('DD/MM/YYYY');
                    }
                    break;
                case 'day':
                    result = plugin.date.format('dddd DD MMMM, YYYY');
                    break;
            }
            return result;
        };

        var rebuild = function() {
            setRange();
            wireframe();
            getData();
        };

        // private methods
        var setRange = function() {
            var start, end;
            switch (plugin.view) {
                case 'month':
                    start = moment(plugin.date).startOf('month').startOf('isoWeek');
                    end = moment(plugin.date).endOf('month').endOf('isoWeek');
                    break;
                case 'week':
                    start = moment(plugin.date).startOf('isoWeek');
                    end = moment(plugin.date).endOf('isoWeek');
                    break;
                case 'day':
                    start = moment(plugin.date);
                    end = moment(plugin.date);
                    break;
            }
            plugin.range = moment().range(start, end);
        };

        var initEvents = function() {
            $element.on('click', '.fc-cell-date', function(event) {
                event.preventDefault();
                event.stopPropagation();
                plugin.view = 'day';
                plugin.goto($(this).parents('.fc-cell').data('date'));
            });

            $element.on('click', '.fc-event', function(event) {
                event.preventDefault();
                event.stopPropagation();
                plugin.settings.onEventClick($(this));
            });

            $element.on('mousedown', '.fc-cell', function(event) {
                event.preventDefault();
                var targetEvent = $(event.target).hasClass('fc-event') || $(event.target).parents('.fc-event').length > 0,
                    $target = null;

                if (!targetEvent) {
                    if ($(event.target).hasClass('fc-cell')) {
                        $target = $(event.target);
                    } else {
                        $target = $(this).closest('.fc-cell');
                    }
                }
                if ($target !== null) {
                    plugin.mousedown = true;
                    var m = moment($target.data('date'));
                    plugin.highlightrange = moment().range(m, m);
                    $target.addClass('fc-cell-highlight');
                }
            });
            $(document).on('mouseup', function(event) {
                event.preventDefault();

                if (plugin.mousedown) {
                    plugin.settings.onDaySelect(plugin.highlightrange);
                    plugin.mousedown = false;
                    delete plugin.highlightrange;
                    $element.find('.fc-cell').removeClass('fc-cell-highlight');
                }

            });
            $element.on('mouseover', '.fc-cell', function(event) {
                event.preventDefault();
                if (plugin.mousedown) {
                    var m = moment($(this).data('date'));
                    if (m.isBefore(plugin.highlightrange.start)) {
                        m = moment(plugin.highlightrange.start);
                    }
                    plugin.highlightrange = moment().range(plugin.highlightrange.start, m);

                    // Highlight cells in the range
                    var thisOne = false;
                    $element.find('.fc-cell').each(function(index, el) {
                        m = moment($(this).data('date'));
                        if (m.within(plugin.highlightrange)) {
                            $(this).addClass('fc-cell-highlight');
                        } else {
                            $(this).removeClass('fc-cell-highlight');
                        }
                    });
                }
            });

            $(window).smartresize(function(e) {
                // On window resize
                if (plugin.view == 'day') {
                    reorganizeDayEvents();
                } else {
                    cellsHeight();
                }
            });
        };

        var wireframe = function() {
            $element.empty();
            $element.addClass('fc-calendar-container');
            var template = templates[plugin.view],
                $head = $(template.head),
                $body = $(template.body);
            $element.attr('data-view', plugin.view);

            var m, done, isEndRow, zIndex, $row;
            switch (plugin.view) {

                case 'month':

                    // Weekday headers
                    m = moment().startOf('isoWeek');
                    done = false;
                    while (!done) {
                        $headcell = $(template.headcell);
                        $headcell.text(m.format('ddd'));
                        $head.append($headcell);
                        if (m.day() == moment().endOf('isoWeek').day()) {
                            done = true;
                        }
                        m.add('days', 1);
                    }

                    // Month day cells
                    isEndRow = false;
                    zIndex = 500;
                    plugin.range.by('days', function(date) {
                        if (date.day() == moment().startOf('isoWeek').day()) {
                            $row = $(template.row);
                        }
                        if (date.day() == moment().endOf('isoWeek').day()) {
                            isEndRow = true;
                        }
                        if ($row) {
                            $cell = $(template.cell);
                            $cell.find('.fc-cell-date').prepend(date.date()).find('.fc-cell-weekday').text(date.format('ddd'));
                            if (date.month() != plugin.date.month()) {
                                $cell.addClass('fc-cell-prev-month');
                            }
                            if (date.isSame(moment(), 'day')) {
                                $cell.addClass('fc-cell-today');
                            }
                            if ([0, 6].indexOf(date.day()) > -1) {
                                $cell.addClass('fc-cell-weekend');
                            }
                            $cell.attr('data-date', date.format('YYYY-MM-DD'));
                            $cell.css('z-index', zIndex);
                            $row.append($cell);
                        }
                        if (isEndRow) {
                            $body.append($row);
                        }--zIndex;
                    });

                    break;
                case 'week':
                    // Weekday headers
                    m = moment(plugin.date).startOf('isoWeek');
                    done = false;
                    while (!done) {
                        $headcell = $(template.headcell);
                        $headcell.text(m.format('ddd DD/MM'));
                        $head.append($headcell);
                        if (m.day() == moment().endOf('isoWeek').day()) {
                            done = true;
                        }
                        m.add('days', 1);
                    }

                    // Week day cells
                    isEndRow = false;
                    zIndex = 500;
                    plugin.range.by('days', function(date) {
                        if (date.day() == moment().startOf('isoWeek').day()) {
                            $row = $(template.row);
                        }
                        if (date.day() == moment().endOf('isoWeek').day()) {
                            isEndRow = true;
                        }
                        if ($row) {
                            $cell = $(template.cell);
                            $cell.find('.fc-cell-date').prepend(date.date()).find('.fc-cell-weekday').text(date.format('ddd'));
                            if (date.month() != plugin.date.month()) {
                                $cell.addClass('fc-cell-prev-month');
                            }
                            if (date.isSame(moment(), 'day')) {
                                $cell.addClass('fc-cell-today');
                            }
                            if ([0, 6].indexOf(date.day()) > -1) {
                                $cell.addClass('fc-cell-weekend');
                            }
                            $cell.attr('data-date', date.format('YYYY-MM-DD'));
                            $cell.css('z-index', zIndex);
                            $row.append($cell);
                        }
                        if (isEndRow) {
                            $body.append($row);
                        }--zIndex;
                    });

                    break;

                case 'day':
                    if (plugin.date.isSame(moment(), 'day')) {
                        $body.addClass('fc-today');
                    }
                    var $tbody = $body.find('.fc-day-body');
                    for (var h = 0; h < 24; h++) {
                        var $fullhour = $(template.fullhour);
                        $fullhour.find('span').append(h + ':00');
                        $fullhour.find('.fc-day-right').attr('data-hour', h + ':00');
                        $tbody.append($fullhour);
                        var $halfhour = $(template.halfhour);
                        $halfhour.find('.fc-day-right').attr('data-hour', h + ':30');
                        $tbody.append($halfhour);
                    }
                    $tbody.append('<div class="fc-day-hour"><div class="fc-day-left"><span></span></div><div class="fc-day-right" data-hour="24:00"></div></div>');
                    break;
            }

            $body.find('.fc-cell').disableSelection();
            $element.empty().append(templates.main);
            $element.find('.fc-calendar').append($head, $body);

            if (plugin.view == 'day') {
                // Add "now" line for Today
                if ($element.find('.fc-body.fc-today').length > 0) {
                    var now = moment();
                    var nowH = now.hours();
                    // Convert minutes into a percentage
                    var minutePercent = Math.round((now.minutes() / 60) * 100) / 100;
                    var $nowHourSection = $element.find('.fc-day-right[data-hour="' + nowH + ':00"]');
                    var lineTop = Math.ceil($nowHourSection.position().top + minutePercent * $nowHourSection.outerHeight());
                    var $nowLine = $(template.nowline);
                    $tbody.append($nowLine);
                    $nowLine.css('top', lineTop + 'px');
                }
            }
        };

        var showLoading = function() {
            var $loading = $(templates.loading);
            $loading.append(plugin.settings.loading);
            $element.append($loading);
        };

        var hideLoading = function() {
            $element.find('.fc-loading').remove();
        };

        /**
         *
         * Get data with Ajax or use the data passed to the settings
         *
         **/
        var getData = function() {
            showLoading();
            plugin.settings.onTitleChange();

            if (plugin.settings.url !== null) {
                var params = plugin.urlParams;
                params.from = plugin.range.start.format('YYYY-MM-DD');
                params.to = plugin.range.end.format('YYYY-MM-DD');
                $.ajax({
                    url: plugin.settings.url,
                    type: 'GET',
                    data: params,
                }).done(function(response) {
                    if (typeof response == 'object') {
                        plugin.events = response;
                        if (plugin.events.length <= plugin.settings.totalMaxEvents) {
                            sortEventsChronologically();
                            fill[plugin.view].call();
                            hideLoading();
                        } else {
                            plugin.settings.onTooManyEvents(plugin.settings.totalMaxEvents, plugin.events.length);
                            reset[plugin.view].call();
                            hideLoading();
                        }
                    } else if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                        //the json is ok
                        plugin.events = $.parseJSON(response);
                        sortEventsChronologically();
                        fill[plugin.view].call();
                        hideLoading();
                    } else {
                        //the json is not ok
                        plugin.settings.onInvalidJSON(response);
                    }
                }).fail(function(response) {
                    if (plugin.settings.debug) {
                        log("error");
                        log(response);
                    }
                    plugin.settings.onAjaxError(response);
                });
            } else if (fill[plugin.view]) {
                sortEventsChronologically();
                fill[plugin.view].call();
                hideLoading();
            }
        };

        var sortEventsChronologically = function() {
            plugin.events.sort(compareEvents);
        };

        var compareEvents = function(a, b) {
            a = moment(a.from);
            b = moment(b.from);
            if (a.isBefore(b)) {
                return -1;
            }
            if (b.isBefore(a)) {
                return 1;
            }
            return 0;
        };

        var fill = {
            month: function() {
                fillMonth();
            },
            week: function() {
                fillMonth();
            },
            day: function() {
                fillDay();
            }
        };
        var reset = {
            month: function() {
                $element.find('.fc-events').empty();
            },
            week: function() {
                $element.find('.fc-events').empty();
            },
            day: function() {
                $element.find('.fc-day-events-full').empty();
                $element.find('.fc-day-events-timed > div').empty();
            }
        };

        var fillMonth = function() {
            // Reset
            reset.month.call();
            // Fill
            plugin.events.forEach(function(ev, index, array) {
                var start = moment(ev.from),
                    end = moment(ev.to),
                    eventRange = moment().range(start, end),
                    top = 0,
                    date;

                eventRange.by('days', function(date) {
                    if (moment(date, 'YYYY-MM-DD').within(plugin.range)) {

                        var $events = $element.find('.fc-cell[data-date=' + date.format('YYYY-MM-DD') + '] .fc-events');
                        if ($events.length > 0) {

                            $ev = $(templates.events.container);
                            if (ev.id !== undefined) {
                                $ev.attr('data-id', ev.id);
                            } else {
                                $ev.attr('data-id', 'fc-event-' + index);
                            }
                            $label = $(templates.events.label);
                            $label.html(ev.label);

                            $ev.addClass(ev.class);
                            if (start.format('H:mm') != '0:00' || end.format('H:mm') != '23:59') {
                                $ev.addClass('timed');

                                $label.prepend(start.format('HH:mm '));
                            }
                            var labelSuffix = ' (';
                            labelSuffix += start.format('D MMM');
                            if (!end.isSame(start, 'year')) {
                                labelSuffix += start.format(' YYYY');
                            }
                            if (!end.isSame(start, 'day')) {
                                labelSuffix += ' - ' + end.format('D MMM');
                            }
                            labelSuffix += end.format(' YYYY') + ')';

                            $ev.attr('title', $label.html() + labelSuffix);
                            if  (typeof $().tooltip == 'function') {
                                $ev.attr('data-toggle', 'tooltip');
                            }

                            $ev.append($label);

                            if (start.isSame(end, 'day')) {
                                $ev.addClass('fc-event-single');
                            } else {
                                // Multiple day event
                                if (date.isSame(start, 'day')) {
                                    // First day of event
                                    $ev.addClass('fc-event-start');
                                } else if (date.isSame(end, 'day')) {
                                    // Last day of event
                                    $ev.addClass('fc-event-end');
                                } else {
                                    $ev.addClass('fc-event-middle');
                                }
                                if (!date.isSame(start, 'day') && date.day() == moment().startOf('isoWeek').day()) {
                                    // Not first day of the event, but first day of the week
                                    $ev.addClass('fc-event-first-weekday');
                                }
                                if (!date.isSame(end, 'day') && date.day() == moment().endOf('isoWeek').day()) {
                                    // Not last day of the event, but last day of the week
                                    $ev.addClass('fc-event-last-weekday');
                                }
                            }
                            // Position
                            if (date.isSame(start, 'day')) {
                                top = 0;
                            } else if (!start.within(plugin.range) && $ev.hasClass('fc-event-first-weekday')) {
                                top = 0;
                            }
                            $ev.css('top', top + 'px');
                            $events.append($ev);
                            // Place event and remember its position for the following cells
                            top = positionMonthEvent($ev, $events);
                        }
                    }
                });
            });
            cellsHeight();
            if  (typeof $().tooltip == 'function') {
                $('[data-toggle=tooltip]').tooltip();
            }
        };

        var fillDay = function() {
            var $fullDayEvents = $element.find('.fc-day-events-full'),
                $timedDayEvents = $element.find('.fc-day-events-timed > div');
            // Reset
            reset.day.call();
            // Fill
            plugin.events.forEach(function(ev, index, array) {
                var start = moment(ev.from),
                    end = moment(ev.to),
                    range = moment().range(start, end),
                    top = 0,
                    $ev;
                range.by('days', function(date) {
                    if (date.isSame(plugin.date, 'day')) {

                        $ev = $(templates.events.container);
                        if (ev.id !== undefined) {
                            $ev.attr('data-id', ev.id);
                        } else {
                            $ev.attr('data-id', 'fc-event-' + index);
                        }
                        $label = $(templates.events.label);
                        $label.html(ev.label);

                        $ev.addClass(ev.class);
                        if (start.format('H:mm') != '0:00' || end.format('H:mm') != '23:59') {
                            $ev.addClass('timed');

                            $label.prepend(start.format('HH:mm - ') + end.format('HH:mm '));
                        }

                        $ev.append($label);

                        if ($ev.hasClass('timed')) {
                            $timedDayEvents.append($ev);
                            positionDayEvent($ev, $timedDayEvents, start, end);
                        } else {
                            $fullDayEvents.append($ev);
                            $fullDayEvents.height($fullDayEvents.find('.fc-event').length * 20);
                        }

                    }
                });
            });

            // Scroll automatically to the hour defined in options
            var $hourRow = $element.find('.fc-day-right[data-hour="' + plugin.settings.dayTopHour + '"]');
            $element.find('.fc-day-body').scrollTop($hourRow.position().top - 15);
        };

        /**
         *
         * Give cells the appropriate height
         *
         **/

        var cellsHeight = function() {
            if (window.innerWidth < 768) {
                $element.addClass('mobile');
            } else {
                $element.removeClass('mobile');
            }

            var totalHeight = $element.find('.fc-head').eq(0).outerHeight();
            var minHeightCSS = $('.fc-cell').eq(0).css('min-height');
            var minCellHeight = minHeightCSS != '' ? parseInt(minHeightCSS.replace('px', '')) : 0,
                cellDateHeight = 0,
                maxCellHeight = 0,
                calculatedHeight;
            $element.find('.fc-row').each(function(index, el) {
                maxCellHeight = 0;
                var $row = $(this);
                $row.find('.fc-cell').each(function(index, el) {
                    var nbEvents = $(this).find('.fc-event').length;
                    if ($(this).outerHeight() < minCellHeight || minCellHeight == 0) {
                        minCellHeight = $(this).outerHeight();
                    }
                    if (nbEvents > 0) {
                        cellDateHeight = $(this).find('.fc-cell-date').outerHeight();
                        calculatedHeight = nbEvents * ($(this).find('.fc-event').eq(0).outerHeight() + 1) + cellDateHeight + 50;
                        if ($element.hasClass('mobile')) {
                            $(this).height(calculatedHeight);
                        } else {
                            if (calculatedHeight > minCellHeight && calculatedHeight > maxCellHeight) {
                                maxCellHeight = calculatedHeight;
                            }
                        }
                    }
                });
                if ($element.hasClass('mobile')) { /* Each cell gets the height corresponding to the number of events it contains (or at least the minimum height) */
                    var allCellHeight = 0;
                    $row.find('.fc-cell').each(function(index, el) {
                        allCellHeight += $(this).outerHeight();
                    });
                    $row.height(allCellHeight);
                } else { /* Each cell gets the maximum height of the row */
                    if (maxCellHeight > 0) {
                        $row.height(maxCellHeight);
                        $row.find('.fc-cell').height(maxCellHeight);
                    } else {
                        $row.height(minCellHeight);
                        $row.find('.fc-cell').height(minCellHeight);
                    }
                }

                totalHeight += $row.outerHeight();
            });
            $element.height(totalHeight);
        };

        var log = function(val) {
            console.log(val);
        };

        var positionMonthEvent = function($eventItem, $container) {
            var eventHeight = $container.find('.fc-event').eq(0).outerHeight() + 2,
                top = parseInt($eventItem.css('top').replace('px', ''));
            // Look for the first completely free space available
            var spaceFound = false;
            var overlap;
            var failsafe = 0;
            while (!spaceFound && failsafe < plugin.settings.failsafeMaxPerDay) {
                overlap = false;
                $container.find('.fc-event').each(function(index, el) {
                    if ($(this).data('id') != $eventItem.data('id') && overlaps($eventItem, $(this))) {
                        overlap = true;
                        return;
                    }
                });
                if (overlap) {
                    top += eventHeight;
                    $eventItem.css('top', top + 'px');
                } else {
                    spaceFound = true;
                }
                failsafe++;
            }
            return top;
        };

        var positionDayEvent = function($eventItem, $container, start, end) {
            var $fromHourRow = $element.find('.fc-day-right[data-hour="' + start.hours() + ':00"]').parent();
            var $toHourRow = $element.find('.fc-day-right[data-hour="' + end.hours() + ':00"]').parent();

            // Convert minutes into a percentage
            var fromMinutePercent = Math.round((start.minutes() / 60) * 100) / 100;
            var toMinutePercent = Math.round((end.minutes() / 60) * 100) / 100;

            var fromTop = $fromHourRow.position().top - 20 + fromMinutePercent * $fromHourRow.height();
            var toTop = $toHourRow.position().top - 20 + toMinutePercent * $toHourRow.height();

            var height = toTop - fromTop;
            var left = $element.find('.day-table .fc-day-left').width() + 10;

            $eventItem.css('top', fromTop + 'px').height(height);

            // Look for the first completely free space available
            var spaceFound = false;
            var overlap;
            var failsafe = 0;
            while (!spaceFound && failsafe < 10) {
                overlap = false;
                $container.find('.fc-event').each(function(index, el) {
                    if ($(this).data('id') != $eventItem.data('id') && overlaps($eventItem, $(this))) {
                        overlap = true;
                        return;
                    }
                });
                if (overlap) {
                    left += $eventItem.width() + 5;
                    $eventItem.css('left', left + 'px');
                } else {
                    spaceFound = true;
                }
                failsafe++;
            }

            plugin.dayEventWidth = $eventItem.width();
        };

        var reorganizeDayEvents = function() {
            $element.find('.fc-day-events-timed > div').css('height', ($element.find('.fc-body').height() - $element.find('.fc-day-full-events').height()) + 'px');
            var eventWidth = $element.find('.fc-day-events-timed .fc-event').eq(0).width();
            if (eventWidth != plugin.dayEventWidth) {
                plugin.dayEventWidth = eventWidth;
                $element.find('.fc-day-events-timed .fc-event').css('left', '-1000px');
                $element.find('.fc-day-events-timed .fc-event').each(function(index, el) {
                    var left = $element.find('.day-table .fc-day-left').width() + 10;
                    var $eventItem = $(this);
                    $eventItem.css('left', left + 'px');
                    // Look for the first completely free space available
                    var spaceFound = false;
                    var overlap;
                    var failsafe = 0;
                    while (!spaceFound && failsafe < 10) {
                        overlap = false;
                        $element.find('.fc-day-events-timed .fc-event').each(function(index, el) {
                            if ($(this).data('id') != $eventItem.data('id') && overlaps($eventItem, $(this))) {
                                overlap = true;
                                return;
                            }
                        });
                        if (overlap) {
                            left += $eventItem.width() + 5;
                            $eventItem.css('left', left + 'px');
                        } else {
                            spaceFound = true;
                        }
                        failsafe++;
                    }
                });
            }
        };

        var templates = {
            main: '<div class="fc-calendar"></div>',
            month: {
                head: '<div class="fc-head"></div>',
                headcell: '<div></div>',
                body: '<div class="fc-body"></div>',
                row: '<div class="fc-row"></div>',
                cell: '<div class="fc-cell"><div class="fc-cell-date"><span class="fc-cell-weekday"></span></div><div class="fc-events"></div></div>'
            },
            week: {
                head: '<div class="fc-head"></div>',
                headcell: '<div></div>',
                body: '<div class="fc-body"></div>',
                row: '<div class="fc-row"></div>',
                cell: '<div class="fc-cell"><div class="fc-cell-date"><span class="fc-cell-weekday"></span></div><div class="fc-events"></div></div>'
            },
            day: {
                head: '',
                headcell: '<div class="fc-head"></div>',
                body: '<div class="fc-body"><div class="day-table"><div class="fc-day-events-full"></div><div class="fc-day-body"><div class="fc-day-events-timed"><div></div></div></div></div>',
                nowline: '<div class="fc-day-now"></div>',
                fullhour: '<div class="fc-day-hour"><div class="fc-day-left"><span></span></div><div class="fc-day-right" data-hour=""></div></div>',
                halfhour: '<div class="fc-day-hour fc-half-hour"><div class="fc-day-left"><span></span></div><div class="fc-day-right" data-hour=""></div></div>'
            },
            events: {
                container: '<div class="fc-event"></div>',
                label: '<span class="fc-event-label"></span>'
            },
            loading: '<div class="fc-loading"></div>'
        };

        // Found at http://jsfiddle.net/98sAG/
        var overlaps = (function() {
            function getPositions(elem) {
                var pos, width, height;
                pos = $(elem).position();
                width = $(elem).width();
                height = $(elem).height();
                return [
                    [pos.left, pos.left + width],
                    [pos.top, pos.top + height]
                ];
            }

            function comparePositions(p1, p2) {
                var r1, r2;
                r1 = p1[0] < p2[0] ? p1 : p2;
                r2 = p1[0] < p2[0] ? p2 : p1;
                return r1[1] > r2[0] || r1[0] === r2[0];
            }

            return function(a, b) {
                var pos1 = getPositions(a),
                    pos2 = getPositions(b);
                return comparePositions(pos1[0], pos2[0]) && comparePositions(pos1[1], pos2[1]);
            };
        })();

        // fire up the plugin!
        // call the "constructor" method
        plugin.init();

    };

    // add the plugin to the jQuery.fn object
    $.fn.naCalendar = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined === $(this).data('naCalendar')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.naCalendar(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('naCalendar').publicMethod(arg1, arg2, ... argn) or
                // element.data('naCalendar').settings.propertyName
                $(this).data('naCalendar', plugin);

            }

        });

    };

})(jQuery);

(function($, sr) {

    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function(func, threshold, execAsap) {
        var timeout;

        return function debounced() {
            var obj = this,
                args = arguments;

            function delayed() {
                if (!execAsap) func.apply(obj, args);
                timeout = null;
            }

            if (timeout) clearTimeout(timeout);
            else if (execAsap) func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    };
    // smartresize 
    jQuery.fn[sr] = function(fn) {
        return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
    };

})(jQuery, 'smartresize');

(function($) {
    $.fn.disableSelection = function() {
        return this.attr('unselectable', 'on').css('user-select', 'none').on('selectstart', false);
    };
    $.fn.enableSelection = function() {
        return this.attr('unselectable', 'off').css('user-select', 'all').on('selectstart', true);
    };
})(jQuery);