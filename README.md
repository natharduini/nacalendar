# naCalendar #

jQuery plugin

### Presentation ###

* Responsive calendar with month/week/day views
* Version: 0.1.0

### Dependencies ###

* jQuery (should be fine with 1.9+)
* Moment.js
* Moment Range plugin